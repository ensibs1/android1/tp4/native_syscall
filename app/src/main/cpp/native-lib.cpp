#include <jni.h>
#include <string>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <signal.h>
#include <setjmp.h>

jmp_buf point; /* set context */
static void handler(int sig, siginfo_t *dont_care, void *dont_care_either)
{
    longjmp(point, 1); /* return to old context */
}
extern "C"
JNIEXPORT void JNICALL
Java_com_tp4_native_1syscall_MainActivity_syscall(JNIEnv *env, jobject thiz, jint syscall_no) {

    struct sigaction sa;

    sigemptyset(&sa.sa_mask);

    sa.sa_flags     = SA_SIGINFO;
    sa.sa_sigaction = handler; /* action to handle SIG_SYS */

    sigaction(SIGSYS, &sa, NULL); /* ignore whether it works or not */

    if (setjmp(point) == 0)
        syscall(syscall_no); /* forbidden syscall */
    else
        fprintf(stderr, "rather unexpected error\n");
}