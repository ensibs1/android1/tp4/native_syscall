package com.tp4.native_syscall;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.tp4.native_syscall.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native_syscall' library on application startup.
    static {
        System.loadLibrary("native_syscall");
    }

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * A native method that is implemented by the 'native_syscall' native library,
     * which is packaged with this application.
     */
    public void sendSyscall(View view) {
        EditText text = (EditText)findViewById(R.id.editCommand);
        String value = text.getText().toString();
        syscall(Integer.parseInt(value));
    }

    private native void syscall(int syscall_no);
}